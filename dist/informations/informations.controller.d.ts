import { InformationsService } from "./informations.service";
import { CreateInformationDto } from "./dtos/create-information.dto";
export declare class InformationsController {
    private informationsService;
    constructor(informationsService: InformationsService);
    getAllInformations(): Promise<import("./information.entity").Information[]>;
    createInformation(body: CreateInformationDto): Promise<import("./information.entity").Information>;
    getSingleInformation(id: string): Promise<import("./information.entity").Information>;
    updateInformation(body: CreateInformationDto, id: string): Promise<import("./information.entity").Information>;
    deleteInformation(id: string): Promise<void>;
}
