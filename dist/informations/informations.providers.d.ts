import { Information } from "./information.entity";
export declare const informationsProviders: {
    provide: string;
    useValue: typeof Information;
}[];
