"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.informationsProviders = void 0;
const information_entity_1 = require("./information.entity");
exports.informationsProviders = [
    {
        provide: "INFORMATIONS_REPOSITORY",
        useValue: information_entity_1.Information,
    },
];
//# sourceMappingURL=informations.providers.js.map