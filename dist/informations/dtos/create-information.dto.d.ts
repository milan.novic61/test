export declare class CreateInformationDto {
    title: string;
    description: string;
}
