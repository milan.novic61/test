"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InformationsService = void 0;
const common_1 = require("@nestjs/common");
let InformationsService = class InformationsService {
    constructor(informationsRepository) {
        this.informationsRepository = informationsRepository;
    }
    async findAll() {
        return this.informationsRepository.findAll();
    }
    async findOne(id) {
        const information = await this.informationsRepository.findByPk(id);
        if (!information) {
            throw new common_1.NotFoundException();
        }
        return information;
    }
    async create(title, description) {
        const information = await this.informationsRepository.build({
            title,
            description,
        });
        return await information.save();
    }
    async update(id, data) {
        const information = await this.findOne(id);
        const keysOfObject = Object.keys(data);
        keysOfObject.map((key) => {
            if (data[key]) {
                information[key] = data[key];
            }
        });
        return await information.save();
    }
    async delete(id) {
        const information = await this.findOne(id);
        await information.destroy();
    }
};
InformationsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("INFORMATIONS_REPOSITORY")),
    __metadata("design:paramtypes", [Object])
], InformationsService);
exports.InformationsService = InformationsService;
//# sourceMappingURL=informations.service.js.map