"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InformationsModule = void 0;
const common_1 = require("@nestjs/common");
const informations_controller_1 = require("./informations.controller");
const informations_service_1 = require("./informations.service");
const informations_providers_1 = require("./informations.providers");
const database_module_1 = require("../database/database.module");
let InformationsModule = class InformationsModule {
};
InformationsModule = __decorate([
    (0, common_1.Module)({
        imports: [database_module_1.DatabaseModule],
        controllers: [informations_controller_1.InformationsController],
        providers: [informations_service_1.InformationsService, ...informations_providers_1.informationsProviders],
    })
], InformationsModule);
exports.InformationsModule = InformationsModule;
//# sourceMappingURL=informations.module.js.map