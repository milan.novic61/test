"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InformationsController = void 0;
const common_1 = require("@nestjs/common");
const informations_service_1 = require("./informations.service");
const create_information_dto_1 = require("./dtos/create-information.dto");
const at_guard_1 = require("../common/guards/at.guard");
const swagger_1 = require("@nestjs/swagger");
let InformationsController = class InformationsController {
    constructor(informationsService) {
        this.informationsService = informationsService;
    }
    getAllInformations() {
        return this.informationsService.findAll();
    }
    createInformation(body) {
        return this.informationsService.create(body.title, body.description);
    }
    getSingleInformation(id) {
        return this.informationsService.findOne(id);
    }
    updateInformation(body, id) {
        return this.informationsService.update(id, body);
    }
    deleteInformation(id) {
        return this.informationsService.delete(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiOkResponse)({ description: "All Informations fetched successfully" }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], InformationsController.prototype, "getAllInformations", null);
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiCreatedResponse)({ description: "Information successfully added" }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: '"title must be a string",\n' +
            '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
            '        "description must be a string",\n' +
            '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_information_dto_1.CreateInformationDto]),
    __metadata("design:returntype", void 0)
], InformationsController.prototype, "createInformation", null);
__decorate([
    (0, common_1.Get)("/:id"),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Information not found" }),
    (0, swagger_1.ApiOkResponse)({ description: "Information fetched from database" }),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], InformationsController.prototype, "getSingleInformation", null);
__decorate([
    (0, common_1.Patch)("/:id"),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Information not found" }),
    (0, swagger_1.ApiOkResponse)({ description: "Information successfully updated" }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: '"title must be a string",\n' +
            '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
            '        "description must be a string",\n' +
            '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
    }),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_information_dto_1.CreateInformationDto, String]),
    __metadata("design:returntype", void 0)
], InformationsController.prototype, "updateInformation", null);
__decorate([
    (0, common_1.Delete)("/:id"),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Information not found" }),
    (0, swagger_1.ApiOkResponse)({ description: "Information successfully deleted " }),
    __param(0, (0, common_1.Param)("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], InformationsController.prototype, "deleteInformation", null);
InformationsController = __decorate([
    (0, swagger_1.ApiTags)("Informations"),
    (0, common_1.Controller)("informations"),
    __metadata("design:paramtypes", [informations_service_1.InformationsService])
], InformationsController);
exports.InformationsController = InformationsController;
//# sourceMappingURL=informations.controller.js.map