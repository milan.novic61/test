import { Model } from "sequelize-typescript";
export declare class Information extends Model {
    id: string;
    title: string;
    description: string;
}
