import { Information } from "./information.entity";
import { CreateInformationDto } from "./dtos/create-information.dto";
export declare class InformationsService {
    private informationsRepository;
    constructor(informationsRepository: typeof Information);
    findAll(): Promise<Information[]>;
    findOne(id: string): Promise<Information>;
    create(title: string, description: string): Promise<Information>;
    update(id: string, data: CreateInformationDto): Promise<Information>;
    delete(id: string): Promise<void>;
}
