import { AuthService } from "./auth.service";
import { AuthDto } from "./dtos/auth.dto";
import { Request } from "express";
import { SignInDto } from "./dtos/sign-in.dto";
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signUp(body: AuthDto): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    signIn(body: SignInDto): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    logout(req: Request): Promise<void>;
    refresh(req: Request): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
}
