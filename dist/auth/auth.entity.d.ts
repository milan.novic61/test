import { Model } from "sequelize-typescript";
export declare class Auth extends Model {
    id: string;
    email: string;
    password: string;
    hashedRt: string;
}
