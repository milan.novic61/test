import { Auth } from "./auth.entity";
import { AuthDto } from "./dtos/auth.dto";
import { JwtService } from "@nestjs/jwt";
export declare class AuthService {
    private authRepository;
    private jwtService;
    constructor(authRepository: typeof Auth, jwtService: JwtService);
    findOne(email: string): Promise<Auth>;
    findByPk(userId: string): Promise<Auth>;
    create(data: AuthDto): Promise<Auth>;
    signUp(data: AuthDto): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    signIn(data: AuthDto): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    logout(userId: string): Promise<void>;
    refreshToken(userId: string, rt: string): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    hashData(data: string): Promise<string>;
    getTokens(userId: string, email: string): Promise<{
        access_token: string;
        refresh_token: string;
    }>;
    updateRtHash(userId: string, rt: string): Promise<void>;
}
