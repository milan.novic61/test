"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const bcrypt = require("bcrypt");
let AuthService = class AuthService {
    constructor(authRepository, jwtService) {
        this.authRepository = authRepository;
        this.jwtService = jwtService;
    }
    async findOne(email) {
        const user = await this.authRepository.findOne({ where: { email } });
        if (!user) {
            throw new common_1.NotFoundException();
        }
        return user;
    }
    async findByPk(userId) {
        const user = await this.authRepository.findOne({ where: { id: userId } });
        if (!user) {
            throw new common_1.NotFoundException();
        }
        return user;
    }
    async create(data) {
        const user = await this.authRepository.findOne({
            where: { email: data.email },
        });
        if (user) {
            throw new common_1.ConflictException("Email already used");
        }
        const newUser = await this.authRepository.build({
            email: data.email,
            password: data.password,
        });
        return await newUser.save();
    }
    async signUp(data) {
        data.password = await this.hashData(data.password);
        const user = await this.create({
            email: data.email,
            password: data.password,
        });
        const tokens = await this.getTokens(user.id, user.email);
        await this.updateRtHash(user.id, tokens.refresh_token);
        return tokens;
    }
    async signIn(data) {
        const user = await this.findOne(data.email);
        const pepperedPassword = data.password + process.env.PEPPER;
        const isValidPassword = await bcrypt.compare(pepperedPassword, user.password);
        if (!isValidPassword) {
            throw new common_1.ForbiddenException("Invalid email or password");
        }
        const tokens = await this.getTokens(user.id, user.email);
        await this.updateRtHash(user.id, tokens.refresh_token);
        return tokens;
    }
    async logout(userId) {
        const user = await this.findByPk(userId);
        if (user.hashedRt) {
            user.hashedRt = null;
            await user.save();
        }
    }
    async refreshToken(userId, rt) {
        const user = await this.findByPk(userId);
        if (!user.hashedRt) {
            throw new common_1.ForbiddenException("User signed out, invalid refresh token");
        }
        const rtMatches = await bcrypt.compare(rt, user.hashedRt);
        if (!rtMatches) {
            throw new common_1.ForbiddenException();
        }
        const tokens = await this.getTokens(user.id, user.email);
        await this.updateRtHash(user.id, tokens.refresh_token);
        return tokens;
    }
    async hashData(data) {
        const saltRounds = parseInt(process.env.SALT_ROUNDS);
        const pepperedData = data + process.env.PEPPER;
        return await bcrypt.hash(pepperedData, saltRounds);
    }
    async getTokens(userId, email) {
        const [at, rt] = await Promise.all([
            await this.jwtService.signAsync({ sub: userId, email }, {
                expiresIn: 60 * 15,
                secret: process.env.JWT_SECRET_AT,
            }),
            await this.jwtService.signAsync({ sub: userId, email }, {
                expiresIn: 60 * 60 * 24 * 7,
                secret: process.env.JWT_SECRET_RT,
            }),
        ]);
        return {
            access_token: at,
            refresh_token: rt,
        };
    }
    async updateRtHash(userId, rt) {
        const hash = await this.hashData(rt);
        const user = await this.findByPk(userId);
        user.hashedRt = hash;
        await user.save();
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("AUTH_REPOSITORY")),
    __metadata("design:paramtypes", [Object, jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map