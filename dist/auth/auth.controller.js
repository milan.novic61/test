"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const auth_dto_1 = require("./dtos/auth.dto");
const rt_guard_1 = require("../common/guards/rt.guard");
const swagger_1 = require("@nestjs/swagger");
const sign_in_dto_1 = require("./dtos/sign-in.dto");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
    }
    signUp(body) {
        return this.authService.signUp(body);
    }
    signIn(body) {
        return this.authService.signIn(body);
    }
    logout(req) {
        const userId = req.user["sub"];
        return this.authService.logout(userId);
    }
    refresh(req) {
        const userId = req.user["sub"];
        const rt = req.user["refreshToken"];
        return this.authService.refreshToken(userId, rt);
    }
};
__decorate([
    (0, common_1.Post)("/signup"),
    (0, swagger_1.ApiConflictResponse)({ description: "Email already used" }),
    (0, swagger_1.ApiCreatedResponse)({ description: "User signed up successfully" }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: 'email must be an email",\n' +
            '        "password too weak",\n' +
            '        "password must be shorter than or equal to 20 characters",\n' +
            '        "password must be longer than or equal to 8 characters",\n' +
            '        "password must be a string"',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [auth_dto_1.AuthDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "signUp", null);
__decorate([
    (0, common_1.Post)("/signin"),
    (0, swagger_1.ApiCreatedResponse)({ description: "User Registration" }),
    (0, swagger_1.ApiOkResponse)({ description: "User Login" }),
    (0, swagger_1.ApiForbiddenResponse)({ description: "Invalid Credentials" }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: '"email must be an email" ,\n' + '"password must be a string"',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sign_in_dto_1.SignInDto]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "signIn", null);
__decorate([
    (0, common_1.Post)("/signout"),
    (0, common_1.UseGuards)(rt_guard_1.RtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiCreatedResponse)({ description: "User successfully logged out" }),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "logout", null);
__decorate([
    (0, common_1.Post)("/refresh"),
    (0, common_1.UseGuards)(rt_guard_1.RtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiCreatedResponse)({
        description: "Access and Refresh token refreshed successfully",
    }),
    (0, swagger_1.ApiForbiddenResponse)({
        description: "User signed out, invalid refresh token",
    }),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "refresh", null);
AuthController = __decorate([
    (0, swagger_1.ApiTags)("Auth"),
    (0, common_1.Controller)("auth"),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map