import { Auth } from "./auth.entity";
export declare const authProviders: {
    provide: string;
    useValue: typeof Auth;
}[];
