"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authProviders = void 0;
const auth_entity_1 = require("./auth.entity");
exports.authProviders = [
    {
        provide: "AUTH_REPOSITORY",
        useValue: auth_entity_1.Auth,
    },
];
//# sourceMappingURL=auth.providers.js.map