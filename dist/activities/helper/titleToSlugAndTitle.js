"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.titleToSlugAndTitle = void 0;
const titleToSlugAndTitle = (title, numOfactivitiesWithSameTitle) => {
    const newTitle = title.trim().replace(/ +(?= )/g, "");
    const lowerCaseTitle = newTitle.toLowerCase();
    let slug = lowerCaseTitle.replace(/ /g, "-");
    if (numOfactivitiesWithSameTitle > 0) {
        slug = slug + "-" + (numOfactivitiesWithSameTitle + 1);
    }
    return [slug, newTitle];
};
exports.titleToSlugAndTitle = titleToSlugAndTitle;
//# sourceMappingURL=titleToSlugAndTitle.js.map