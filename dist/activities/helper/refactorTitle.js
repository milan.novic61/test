"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refactorTitle = void 0;
const refactorTitle = (title) => {
    return title.trim().replace(/ +(?= )/g, "");
};
exports.refactorTitle = refactorTitle;
//# sourceMappingURL=refactorTitle.js.map