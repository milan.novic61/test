"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.titleToSlug = void 0;
const titleToSlug = (title, numOfactivitiesWithSameTitle) => {
    const lowerCaseTitle = title.toLowerCase();
    let slug = lowerCaseTitle.replace(/ /g, "-");
    if (numOfactivitiesWithSameTitle > 0) {
        slug = slug + "-" + (numOfactivitiesWithSameTitle + 1);
    }
    return slug;
};
exports.titleToSlug = titleToSlug;
//# sourceMappingURL=titleToSlug.js.map