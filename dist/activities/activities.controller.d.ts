import { CreateActivityDto } from "./dtos/create-activity.dto";
import { ActivitiesService } from "./activities.service";
import { GetActivitiesDto } from "./dtos/get-activities.dto";
import { Activity } from "./activity.entity";
export declare class ActivitiesController {
    private activityService;
    constructor(activityService: ActivitiesService);
    getAllActivities(body: GetActivitiesDto): Promise<Activity[]>;
    getSingleActivity(slug: string): Promise<Activity>;
    createActivity(body: CreateActivityDto): Promise<Activity>;
    updateActivity(slug: string, body: Partial<Activity>): Promise<Activity>;
    deleteActivity(slug: string): Promise<void>;
}
