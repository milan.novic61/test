export declare enum OrderByEnum {
    title = "title",
    description = "description",
    cost = "cost",
    tripDuration = "tripDuration"
}
