"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderByEnum = void 0;
var OrderByEnum;
(function (OrderByEnum) {
    OrderByEnum["title"] = "title";
    OrderByEnum["description"] = "description";
    OrderByEnum["cost"] = "cost";
    OrderByEnum["tripDuration"] = "tripDuration";
})(OrderByEnum = exports.OrderByEnum || (exports.OrderByEnum = {}));
//# sourceMappingURL=orderby.js.map