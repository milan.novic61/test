export declare class GetActivitiesDto {
    limit: number;
    offset: number;
    orderBy: string;
    arranging: string;
}
