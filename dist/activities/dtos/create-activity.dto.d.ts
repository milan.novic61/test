export declare class CreateActivityDto {
    title: string;
    description: string;
    cost: number;
    tripDuration: number;
}
