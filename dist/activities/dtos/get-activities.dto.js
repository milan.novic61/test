"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GetActivitiesDto = void 0;
const class_validator_1 = require("class-validator");
const filter_1 = require("../../common/enums/filter");
const orderby_1 = require("../enums/orderby");
const swagger_1 = require("@nestjs/swagger");
class GetActivitiesDto {
}
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Max)(50),
    (0, class_validator_1.Min)(1),
    (0, swagger_1.ApiProperty)({
        type: Number,
        description: "Amount of returned values",
        default: "4",
    }),
    __metadata("design:type", Number)
], GetActivitiesDto.prototype, "limit", void 0);
__decorate([
    (0, class_validator_1.IsNumber)(),
    (0, class_validator_1.Min)(1),
    (0, swagger_1.ApiProperty)({
        type: Number,
        description: "Offset on returning values",
        default: "1",
    }),
    __metadata("design:type", Number)
], GetActivitiesDto.prototype, "offset", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(orderby_1.OrderByEnum, {
        message: "orderBy must be one from the following title, description, cost or tripDuration",
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: "Order of returned values",
        default: "cost",
    }),
    __metadata("design:type", String)
], GetActivitiesDto.prototype, "orderBy", void 0);
__decorate([
    (0, class_validator_1.IsEnum)(filter_1.ArrangingEnum, {
        message: "arranging allowed values are ASC or DESC",
    }),
    (0, swagger_1.ApiProperty)({
        type: String,
        description: "Arrange of returned values",
        default: "DESC",
    }),
    __metadata("design:type", String)
], GetActivitiesDto.prototype, "arranging", void 0);
exports.GetActivitiesDto = GetActivitiesDto;
//# sourceMappingURL=get-activities.dto.js.map