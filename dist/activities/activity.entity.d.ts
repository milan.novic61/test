import { Model } from "sequelize-typescript";
export declare class Activity extends Model {
    id: string;
    title: string;
    description: string;
    cost: number;
    tripDuration: number;
    slug: string;
}
