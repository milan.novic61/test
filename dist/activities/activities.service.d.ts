import { Activity } from "./activity.entity";
export declare class ActivitiesService {
    private activitiesRepository;
    constructor(activitiesRepository: typeof Activity);
    create(title: string, description: string, cost: number, tripDuration: number): Promise<Activity>;
    countActivitiesWithTitle(title: string): Promise<number>;
    findAll(limit: number, offset: number, orderBy: string, arranging: string): Promise<Activity[]>;
    findOne(slug: string): Promise<Activity>;
    update(slug: string, updateObject: Partial<Activity>): Promise<Activity>;
    delete(slug: string): Promise<void>;
}
