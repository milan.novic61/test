"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivitiesController = void 0;
const common_1 = require("@nestjs/common");
const create_activity_dto_1 = require("./dtos/create-activity.dto");
const activities_service_1 = require("./activities.service");
const get_activities_dto_1 = require("./dtos/get-activities.dto");
const at_guard_1 = require("../common/guards/at.guard");
const swagger_1 = require("@nestjs/swagger");
let ActivitiesController = class ActivitiesController {
    constructor(activityService) {
        this.activityService = activityService;
    }
    async getAllActivities(body) {
        return this.activityService.findAll(body.limit, body.offset, body.orderBy, body.arranging);
    }
    getSingleActivity(slug) {
        return this.activityService.findOne(slug);
    }
    async createActivity(body) {
        return this.activityService.create(body.title, body.description, body.cost, body.tripDuration);
    }
    async updateActivity(slug, body) {
        return this.activityService.update(slug, body);
    }
    async deleteActivity(slug) {
        return this.activityService.delete(slug);
    }
};
__decorate([
    (0, common_1.Post)("get"),
    (0, swagger_1.ApiOkResponse)({ description: "All Activities fetched successfully" }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: '"limit must not be less than 1",\n' +
            '        "limit must not be greater than 50",\n' +
            '        "limit must be a number conforming to the specified constraints",\n' +
            '        "offset must not be less than 1",\n' +
            '        "offset must be a number conforming to the specified constraints",\n' +
            '        "orderBy must be one from the following title, description, cost or trip_duration",\n' +
            '        "orderBy must be a string",\n' +
            '        "arranging must be a valid enum value',
    }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [get_activities_dto_1.GetActivitiesDto]),
    __metadata("design:returntype", Promise)
], ActivitiesController.prototype, "getAllActivities", null);
__decorate([
    (0, common_1.Get)("/:slug"),
    (0, swagger_1.ApiOkResponse)({ description: "Activity fetched successfully" }),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Activity not found" }),
    __param(0, (0, common_1.Param)("slug")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ActivitiesController.prototype, "getSingleActivity", null);
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: ' "title must be a string",\n' +
            '        "description must be a string",\n' +
            '        "cost must be a number conforming to the specified constraints",\n' +
            '        "tripDuration must be an integer number"',
    }),
    (0, swagger_1.ApiCreatedResponse)({ description: "Activity successfully created" }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_activity_dto_1.CreateActivityDto]),
    __metadata("design:returntype", Promise)
], ActivitiesController.prototype, "createActivity", null);
__decorate([
    (0, common_1.Patch)("/:slug"),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Activity not found" }),
    (0, swagger_1.ApiOkResponse)({ description: "Activity successfully updated" }),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiBadRequestResponse)({
        description: '"title must be a string",\n' +
            '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
            '        "description must be a string",\n' +
            '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
    }),
    __param(0, (0, common_1.Param)("slug")),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], ActivitiesController.prototype, "updateActivity", null);
__decorate([
    (0, common_1.Delete)("/:slug"),
    (0, common_1.UseGuards)(at_guard_1.AtGuard),
    (0, swagger_1.ApiBearerAuth)(),
    (0, swagger_1.ApiUnauthorizedResponse)({
        description: "Unauthorized access",
    }),
    (0, swagger_1.ApiNotFoundResponse)({ description: "Activities not found" }),
    (0, swagger_1.ApiOkResponse)({ description: "Activities successfully deleted " }),
    __param(0, (0, common_1.Param)("slug")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ActivitiesController.prototype, "deleteActivity", null);
ActivitiesController = __decorate([
    (0, swagger_1.ApiTags)("Activities"),
    (0, common_1.Controller)("activities"),
    __metadata("design:paramtypes", [activities_service_1.ActivitiesService])
], ActivitiesController);
exports.ActivitiesController = ActivitiesController;
//# sourceMappingURL=activities.controller.js.map