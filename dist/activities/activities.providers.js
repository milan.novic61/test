"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.activitiesProviders = void 0;
const activity_entity_1 = require("./activity.entity");
exports.activitiesProviders = [
    {
        provide: "ACTIVITIES_REPOSITORY",
        useValue: activity_entity_1.Activity,
    },
];
//# sourceMappingURL=activities.providers.js.map