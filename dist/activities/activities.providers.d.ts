import { Activity } from "./activity.entity";
export declare const activitiesProviders: {
    provide: string;
    useValue: typeof Activity;
}[];
