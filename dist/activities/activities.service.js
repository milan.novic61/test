"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ActivitiesService = void 0;
const common_1 = require("@nestjs/common");
const titleToSlug_1 = require("./helper/titleToSlug");
const refactorTitle_1 = require("./helper/refactorTitle");
let ActivitiesService = class ActivitiesService {
    constructor(activitiesRepository) {
        this.activitiesRepository = activitiesRepository;
    }
    async create(title, description, cost, tripDuration) {
        title = (0, refactorTitle_1.refactorTitle)(title);
        const numOfActivitiesWithSameTitle = await this.countActivitiesWithTitle(title);
        console.log("***********");
        const slug = (0, titleToSlug_1.titleToSlug)(title, numOfActivitiesWithSameTitle);
        return this.activitiesRepository.create({
            title,
            description,
            cost,
            tripDuration,
            slug,
        });
    }
    async countActivitiesWithTitle(title) {
        const activitiesWithSameTitle = await this.activitiesRepository.findAndCountAll({
            where: {
                title,
            },
        });
        console.log(activitiesWithSameTitle.count);
        return activitiesWithSameTitle.count;
    }
    async findAll(limit, offset, orderBy, arranging) {
        const newOffset = (offset - 1) * limit;
        return this.activitiesRepository.findAll({
            limit,
            order: [[orderBy, arranging]],
            offset: newOffset,
        });
    }
    async findOne(slug) {
        const activity = await this.activitiesRepository.findOne({
            where: {
                slug,
            },
        });
        if (!activity) {
            throw new common_1.NotFoundException();
        }
        else {
            return activity;
        }
    }
    async update(slug, updateObject) {
        const activity = await this.findOne(slug);
        if (updateObject.title) {
            updateObject.title = (0, refactorTitle_1.refactorTitle)(updateObject.title);
            if (updateObject.title !== activity.title) {
                const numOfActivitiesWithSameTitle = await this.countActivitiesWithTitle(updateObject.title);
                updateObject.slug = (0, titleToSlug_1.titleToSlug)(updateObject.title, numOfActivitiesWithSameTitle - 1);
            }
            const keysOfObject = Object.keys(updateObject);
            keysOfObject.map((key) => {
                if (updateObject[key]) {
                    activity[key] = updateObject[key];
                }
            });
        }
        return await activity.save();
    }
    async delete(slug) {
        const activity = await this.findOne(slug);
        return await activity.destroy();
    }
};
ActivitiesService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)("ACTIVITIES_REPOSITORY")),
    __metadata("design:paramtypes", [Object])
], ActivitiesService);
exports.ActivitiesService = ActivitiesService;
//# sourceMappingURL=activities.service.js.map