"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.databaseProviders = void 0;
const sequelize_typescript_1 = require("sequelize-typescript");
const activity_entity_1 = require("../activities/activity.entity");
const information_entity_1 = require("../informations/information.entity");
const auth_entity_1 = require("../auth/auth.entity");
exports.databaseProviders = [
    {
        provide: "SEQUELIZE",
        useFactory: async () => {
            const sequelize = new sequelize_typescript_1.Sequelize({
                dialect: "postgres",
                host: process.env.HOST,
                port: parseInt(process.env.PORT),
                username: process.env.USERNAME,
                password: process.env.PASSWORD,
                database: process.env.DATABASE,
            });
            sequelize.addModels([information_entity_1.Information, activity_entity_1.Activity, auth_entity_1.Auth]);
            await sequelize.sync();
            return sequelize;
        },
    },
];
//# sourceMappingURL=database.providers.js.map