import { Body, Controller, Post, Req, UseGuards } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthDto } from "./dtos/auth.dto";
import { RtGuard } from "../common/guards/rt.guard";
import { Request } from "express";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";
import { SignInDto } from "./dtos/sign-in.dto";

@ApiTags("Auth")
@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  //
  //------------------SIGN UP------------------
  @Post("/signup")
  @ApiConflictResponse({ description: "Email already used" })
  @ApiCreatedResponse({ description: "User signed up successfully" })
  @ApiBadRequestResponse({
    description:
      'email must be an email",\n' +
      '        "password too weak",\n' +
      '        "password must be shorter than or equal to 20 characters",\n' +
      '        "password must be longer than or equal to 8 characters",\n' +
      '        "password must be a string"',
  })
  signUp(@Body() body: AuthDto) {
    return this.authService.signUp(body);
  }

  //
  //------------------SIGN IN------------------
  @Post("/signin")
  @ApiCreatedResponse({ description: "User Registration" })
  @ApiOkResponse({ description: "User Login" })
  @ApiForbiddenResponse({ description: "Invalid Credentials" })
  @ApiBadRequestResponse({
    description: '"email must be an email" ,\n' + '"password must be a string"',
  })
  signIn(@Body() body: SignInDto) {
    return this.authService.signIn(body);
  }

  //
  //------------------SIGN OUT------------------
  @Post("/signout")
  @UseGuards(RtGuard)
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiCreatedResponse({ description: "User successfully logged out" })
  logout(@Req() req: Request) {
    const userId = req.user["sub"];
    return this.authService.logout(userId);
  }

  //
  //------------------REFRESH------------------
  @Post("/refresh")
  @UseGuards(RtGuard)
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiCreatedResponse({
    description: "Access and Refresh token refreshed successfully",
  })
  @ApiForbiddenResponse({
    description: "User signed out, invalid refresh token",
  })
  refresh(@Req() req: Request) {
    const userId = req.user["sub"];
    const rt = req.user["refreshToken"];

    return this.authService.refreshToken(userId, rt);
  }
}
