import {
  ConflictException,
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { Auth } from "./auth.entity";
import { AuthDto } from "./dtos/auth.dto";
import { JwtService } from "@nestjs/jwt";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require("bcrypt");

@Injectable()
export class AuthService {
  constructor(
    @Inject("AUTH_REPOSITORY")
    private authRepository: typeof Auth,
    private jwtService: JwtService
  ) {}

  async findOne(email: string): Promise<Auth> {
    const user = await this.authRepository.findOne({ where: { email } });

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  async findByPk(userId: string): Promise<Auth> {
    const user = await this.authRepository.findOne({ where: { id: userId } });

    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  async create(data: AuthDto) {
    const user = await this.authRepository.findOne({
      where: { email: data.email },
    });

    if (user) {
      throw new ConflictException("Email already used");
    }

    const newUser = await this.authRepository.build({
      email: data.email,
      password: data.password,
    });

    return await newUser.save();
  }

  //SIGN UP
  async signUp(data: AuthDto) {
    data.password = await this.hashData(data.password);

    const user = await this.create({
      email: data.email,
      password: data.password,
    });

    const tokens = await this.getTokens(user.id, user.email);
    await this.updateRtHash(user.id, tokens.refresh_token);
    return tokens;
  }

  //SIGN IN
  async signIn(data: AuthDto) {
    const user: Auth = await this.findOne(data.email);

    const pepperedPassword = data.password + process.env.PEPPER;

    const isValidPassword = await bcrypt.compare(
      pepperedPassword,
      user.password
    );

    if (!isValidPassword) {
      throw new ForbiddenException("Invalid email or password");
    }
    const tokens = await this.getTokens(user.id, user.email);
    await this.updateRtHash(user.id, tokens.refresh_token);
    return tokens;
  }

  async logout(userId: string) {
    const user: Auth = await this.findByPk(userId);
    if (user.hashedRt) {
      user.hashedRt = null;
      await user.save();
    }
  }

  async refreshToken(userId: string, rt: string) {
    const user = await this.findByPk(userId);

    if (!user.hashedRt) {
      throw new ForbiddenException("User signed out, invalid refresh token");
    }
    const rtMatches = await bcrypt.compare(rt, user.hashedRt);

    if (!rtMatches) {
      throw new ForbiddenException();
    }
    const tokens = await this.getTokens(user.id, user.email);
    await this.updateRtHash(user.id, tokens.refresh_token);
    return tokens;
  }

  async hashData(data: string): Promise<string> {
    const saltRounds = parseInt(process.env.SALT_ROUNDS);

    const pepperedData = data + process.env.PEPPER;

    return await bcrypt.hash(pepperedData, saltRounds);
  }

  async getTokens(userId: string, email: string) {
    const [at, rt] = await Promise.all([
      await this.jwtService.signAsync(
        { sub: userId, email },
        {
          expiresIn: 60 * 15,
          secret: process.env.JWT_SECRET_AT,
        }
      ),
      await this.jwtService.signAsync(
        { sub: userId, email },
        {
          expiresIn: 60 * 60 * 24 * 7,
          secret: process.env.JWT_SECRET_RT,
        }
      ),
    ]);
    return {
      access_token: at,
      refresh_token: rt,
    };
  }

  async updateRtHash(userId: string, rt: string) {
    const hash = await this.hashData(rt);
    const user = await this.findByPk(userId);

    user.hashedRt = hash;
    await user.save();
  }
}
