import { IsEmail, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class SignInDto {
  @ApiProperty({
    type: String,
    description: "email",
    default: "email@email.com",
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    type: String,
    description: "Password",
    default: "Password123.",
  })
  @IsString()
  password: string;
}
