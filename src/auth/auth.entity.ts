import {
  Column,
  Default,
  IsEmail,
  IsUUID,
  Model,
  NotNull,
  PrimaryKey,
  Table,
  Unique,
} from "sequelize-typescript";
import { v4 as uuidv4 } from "uuid";

@Table({ tableName: "auth" })
export class Auth extends Model {
  @Default(uuidv4)
  @IsUUID(4)
  @PrimaryKey
  @Column
  id: string;

  @Unique
  @IsEmail
  @NotNull
  @Column({ allowNull: false })
  email: string;

  @NotNull
  @Column({ allowNull: false })
  password: string;

  @Column
  hashedRt: string;
}
