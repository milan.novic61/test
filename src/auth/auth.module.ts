import { Module } from "@nestjs/common";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { authProviders } from "./auth.providers";
import { RtStrategy } from "./strategies/rt.strategy";
import { AtStrategy } from "./strategies/at.strategy";
import { JwtModule } from "@nestjs/jwt";

@Module({
  imports: [JwtModule.register({})],
  controllers: [AuthController],
  providers: [AuthService, ...authProviders, RtStrategy, AtStrategy],
})
export class AuthModule {}
