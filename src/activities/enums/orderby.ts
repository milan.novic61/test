export enum OrderByEnum {
  title = "title",
  description = "description",
  cost = "cost",
  tripDuration = "tripDuration",
}
