import { IsEnum, IsNumber, Max, Min } from "class-validator";
import { ArrangingEnum } from "../../common/enums/filter";
import { OrderByEnum } from "../enums/orderby";
import { ApiProperty } from "@nestjs/swagger";

export class GetActivitiesDto {
  @IsNumber()
  @Max(50)
  @Min(1)
  @ApiProperty({
    type: Number,
    description: "Amount of returned values",
    default: "4",
  })
  limit: number;

  @IsNumber()
  @Min(1)
  @ApiProperty({
    type: Number,
    description: "Offset on returning values",
    default: "1",
  })
  offset: number;

  @IsEnum(OrderByEnum, {
    message:
      "orderBy must be one from the following title, description, cost or tripDuration",
  })
  @ApiProperty({
    type: String,
    description: "Order of returned values",
    default: "cost",
  })
  orderBy: string;

  @IsEnum(ArrangingEnum, {
    message: "arranging allowed values are ASC or DESC",
  })
  @ApiProperty({
    type: String,
    description: "Arrange of returned values",
    default: "DESC",
  })
  arranging: string;
}
