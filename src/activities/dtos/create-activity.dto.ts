import { IsInt, IsNumber, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateActivityDto {
  @IsString()
  @ApiProperty({
    type: String,
    description: "Title of Activity",
    default: "Activity 1",
  })
  title: string;

  @IsString()
  @ApiProperty({
    type: String,
    description: "Description of Activity",
    default: "Description 1",
  })
  description: string;

  @IsNumber()
  @ApiProperty({
    type: Number,
    description: "Cost of Activity",
    default: "50",
  })
  cost: number;

  @IsInt()
  @ApiProperty({
    type: Number,
    description: "Duration of Activity (in days)",
    default: "1",
  })
  tripDuration: number;
}
