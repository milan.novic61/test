import { Module } from "@nestjs/common";
import { ActivitiesController } from "./activities.controller";
import { ActivitiesService } from "./activities.service";
import { DatabaseModule } from "../database/database.module";
import { activitiesProviders } from "./activities.providers";

@Module({
  imports: [DatabaseModule],
  controllers: [ActivitiesController],
  providers: [ActivitiesService, ...activitiesProviders],
})
export class ActivitiesModule {}
