import { Activity } from "./activity.entity";

export const activitiesProviders = [
  {
    provide: "ACTIVITIES_REPOSITORY",
    useValue: Activity,
  },
];
