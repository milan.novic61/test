import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import { CreateActivityDto } from "./dtos/create-activity.dto";
import { ActivitiesService } from "./activities.service";
import { GetActivitiesDto } from "./dtos/get-activities.dto";
import { Activity } from "./activity.entity";
import { AtGuard } from "../common/guards/at.guard";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";

@ApiTags("Activities")
@Controller("activities")
export class ActivitiesController {
  constructor(private activityService: ActivitiesService) {}

  //
  //------------------GET------------------
  @Post("get")
  @ApiOkResponse({ description: "All Activities fetched successfully" })
  @ApiBadRequestResponse({
    description:
      '"limit must not be less than 1",\n' +
      '        "limit must not be greater than 50",\n' +
      '        "limit must be a number conforming to the specified constraints",\n' +
      '        "offset must not be less than 1",\n' +
      '        "offset must be a number conforming to the specified constraints",\n' +
      '        "orderBy must be one from the following title, description, cost or trip_duration",\n' +
      '        "orderBy must be a string",\n' +
      '        "arranging must be a valid enum value',
  })
  async getAllActivities(@Body() body: GetActivitiesDto) {
    return this.activityService.findAll(
      body.limit,
      body.offset,
      body.orderBy,
      body.arranging
    );
  }

  //
  //------------------GET/:id------------------

  @Get("/:slug")
  @ApiOkResponse({ description: "Activity fetched successfully" })
  @ApiNotFoundResponse({ description: "Activity not found" })
  getSingleActivity(@Param("slug") slug: string) {
    return this.activityService.findOne(slug);
  }

  //
  //------------------POST------------------
  @Post()
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiBadRequestResponse({
    description:
      ' "title must be a string",\n' +
      '        "description must be a string",\n' +
      '        "cost must be a number conforming to the specified constraints",\n' +
      '        "tripDuration must be an integer number"',
  })
  @ApiCreatedResponse({ description: "Activity successfully created" })
  async createActivity(@Body() body: CreateActivityDto) {
    return this.activityService.create(
      body.title,
      body.description,
      body.cost,
      body.tripDuration
    );
  }

  //
  //------------------PATCH------------------
  @Patch("/:slug")
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: "Activity not found" })
  @ApiOkResponse({ description: "Activity successfully updated" })
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiBadRequestResponse({
    description:
      '"title must be a string",\n' +
      '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
      '        "description must be a string",\n' +
      '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
  })
  async updateActivity(
    @Param("slug") slug: string,
    @Body() body: Partial<Activity>
  ) {
    return this.activityService.update(slug, body);
  }

  //
  //------------------DELETE------------------
  @Delete("/:slug")
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiNotFoundResponse({ description: "Activities not found" })
  @ApiOkResponse({ description: "Activities successfully deleted " })
  async deleteActivity(@Param("slug") slug: string) {
    return this.activityService.delete(slug);
  }
}
