import {
  Column,
  Default,
  IsUUID,
  Model,
  NotNull,
  PrimaryKey,
  Table,
  Unique,
} from "sequelize-typescript";
import { v4 as uuidv4 } from "uuid";

@Table({ modelName: "activity" })
export class Activity extends Model {
  @Default(uuidv4)
  @IsUUID(4)
  @PrimaryKey
  @Column
  id: string;

  @NotNull
  @Column({ allowNull: false })
  title: string;

  @Column
  description: string;

  @NotNull
  @Column({ allowNull: false })
  cost: number;

  @NotNull
  @Column({ allowNull: false })
  tripDuration: number;

  @Unique
  @NotNull
  @Column({ allowNull: false })
  slug: string;
}
