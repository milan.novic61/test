export const titleToSlug = (
  title: string,
  numOfactivitiesWithSameTitle: number
): string => {
  const lowerCaseTitle = title.toLowerCase();

  let slug = lowerCaseTitle.replace(/ /g, "-");

  if (numOfactivitiesWithSameTitle > 0) {
    slug = slug + "-" + (numOfactivitiesWithSameTitle + 1);
  }

  return slug;
};
