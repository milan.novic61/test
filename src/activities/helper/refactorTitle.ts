export const refactorTitle = (title: string) => {
  return title.trim().replace(/ +(?= )/g, "");
};
