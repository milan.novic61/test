import { Inject, Injectable, NotFoundException } from "@nestjs/common";
import { Activity } from "./activity.entity";
import { titleToSlug } from "./helper/titleToSlug";
import { refactorTitle } from "./helper/refactorTitle";

@Injectable()
export class ActivitiesService {
  constructor(
    @Inject("ACTIVITIES_REPOSITORY")
    private activitiesRepository: typeof Activity
  ) {}

  async create(
    title: string,
    description: string,
    cost: number,
    tripDuration: number
  ) {
    title = refactorTitle(title);

    const numOfActivitiesWithSameTitle = await this.countActivitiesWithTitle(
      title
    );

    const slug = titleToSlug(title, numOfActivitiesWithSameTitle);

    return this.activitiesRepository.create({
      title,
      description,
      cost,
      tripDuration,
      slug,
    });
  }

  async countActivitiesWithTitle(title: string) {
    const activitiesWithSameTitle =
      await this.activitiesRepository.findAndCountAll({
        where: {
          title,
        },
      });

    return activitiesWithSameTitle.count;
  }

  async findAll(
    limit: number,
    offset: number,
    orderBy: string,
    arranging: string
  ) {
    const newOffset = (offset - 1) * limit;

    return this.activitiesRepository.findAll({
      limit,
      order: [[orderBy, arranging]],
      offset: newOffset,
    });
  }

  async findOne(slug: string) {
    const activity = await this.activitiesRepository.findOne({
      where: {
        slug,
      },
    });

    if (!activity) {
      throw new NotFoundException();
    } else {
      return activity;
    }
  }

  async update(slug: string, updateObject: Partial<Activity>) {
    const activity = await this.findOne(slug);

    //Need to change slug according to new title
    if (updateObject.title) {
      updateObject.title = refactorTitle(updateObject.title);
      if (updateObject.title !== activity.title) {
        const numOfActivitiesWithSameTitle =
          await this.countActivitiesWithTitle(updateObject.title);

        updateObject.slug = titleToSlug(
          updateObject.title,
          numOfActivitiesWithSameTitle - 1
        );
      }

      const keysOfObject = Object.keys(updateObject);

      keysOfObject.map((key) => {
        if (updateObject[key]) {
          activity[key] = updateObject[key];
        }
      });
    }

    return await activity.save();
  }

  async delete(slug: string) {
    const activity = await this.findOne(slug);
    return await activity.destroy();
  }
}
