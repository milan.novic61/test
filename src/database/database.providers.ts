import { Sequelize } from "sequelize-typescript";
import { Activity } from "../activities/activity.entity";
import { Information } from "../informations/information.entity";
import { Auth } from "../auth/auth.entity";

export const databaseProviders = [
  {
    provide: "SEQUELIZE",
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: "postgres",
        host: process.env.HOST,
        port: parseInt(process.env.PORT),
        username: process.env.USERNAME,
        password: process.env.PASSWORD,
        database: process.env.DATABASE,
      });
      sequelize.addModels([Information, Activity, Auth]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
