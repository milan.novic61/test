import { Information } from "./information.entity";

export const informationsProviders = [
  {
    provide: "INFORMATIONS_REPOSITORY",
    useValue: Information,
  },
];
