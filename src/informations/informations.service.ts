import { Inject, Injectable, NotFoundException } from "@nestjs/common";
import { Information } from "./information.entity";
import { CreateInformationDto } from "./dtos/create-information.dto";

@Injectable()
export class InformationsService {
  constructor(
    @Inject("INFORMATIONS_REPOSITORY")
    private informationsRepository: typeof Information
  ) {}

  async findAll(): Promise<Information[]> {
    return this.informationsRepository.findAll();
  }

  async findOne(id: string) {
    const information = await this.informationsRepository.findByPk(id);
    if (!information) {
      throw new NotFoundException();
    }

    return information;
  }

  async create(title: string, description: string) {
    const information = await this.informationsRepository.build({
      title,
      description,
    });
    return await information.save();
  }

  async update(id: string, data: CreateInformationDto) {
    const information = await this.findOne(id);

    const keysOfObject = Object.keys(data);

    keysOfObject.map((key) => {
      if (data[key]) {
        information[key] = data[key];
      }
    });
    return await information.save();
  }

  async delete(id: string) {
    const information = await this.findOne(id);

    await information.destroy();
  }
}
