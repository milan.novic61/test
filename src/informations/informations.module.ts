import { Module } from "@nestjs/common";
import { InformationsController } from "./informations.controller";
import { InformationsService } from "./informations.service";
import { informationsProviders } from "./informations.providers";
import { DatabaseModule } from "../database/database.module";

@Module({
  imports: [DatabaseModule],
  controllers: [InformationsController],
  providers: [InformationsService, ...informationsProviders],
})
export class InformationsModule {}
