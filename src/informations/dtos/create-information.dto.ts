import { IsString, Length } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateInformationDto {
  @ApiProperty({
    type: String,
    description: "Title of Information",
    default: "My Title",
  })
  @Length(3, 100)
  @IsString()
  title: string;

  @ApiProperty({
    type: String,
    description: "Description of Information",
    default: "My Description",
  })
  @Length(0, 255)
  @IsString()
  description: string;
}
