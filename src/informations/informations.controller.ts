import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from "@nestjs/common";
import { InformationsService } from "./informations.service";
import { CreateInformationDto } from "./dtos/create-information.dto";
import { AtGuard } from "../common/guards/at.guard";
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from "@nestjs/swagger";

@ApiTags("Informations")
@Controller("informations")
export class InformationsController {
  constructor(private informationsService: InformationsService) {}

  //------------------GET------------------
  @Get()
  @ApiOkResponse({ description: "All Informations fetched successfully" })
  getAllInformations() {
    return this.informationsService.findAll();
  }

  //
  //------------------POST------------------
  @Post()
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse({ description: "Information successfully added" })
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiBadRequestResponse({
    description:
      '"title must be a string",\n' +
      '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
      '        "description must be a string",\n' +
      '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
  })
  createInformation(@Body() body: CreateInformationDto) {
    return this.informationsService.create(body.title, body.description);
  }

  //
  //------------------GET/:id------------------
  @Get("/:id")
  @ApiNotFoundResponse({ description: "Information not found" })
  @ApiOkResponse({ description: "Information fetched from database" })
  getSingleInformation(@Param("id") id: string) {
    return this.informationsService.findOne(id);
  }

  //
  //------------------PATCH------------------
  @Patch("/:id")
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: "Information not found" })
  @ApiOkResponse({ description: "Information successfully updated" })
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiBadRequestResponse({
    description:
      '"title must be a string",\n' +
      '        "title must be longer than or equal to 3 and shorter than or equal to 100 characters",\n' +
      '        "description must be a string",\n' +
      '        "description must be longer than or equal to 0 and shorter than or equal to 255 characters"',
  })
  updateInformation(
    @Body() body: CreateInformationDto,
    @Param("id") id: string
  ) {
    return this.informationsService.update(id, body);
  }

  //
  //------------------DELETE------------------
  @Delete("/:id")
  @UseGuards(AtGuard)
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({
    description: "Unauthorized access",
  })
  @ApiNotFoundResponse({ description: "Information not found" })
  @ApiOkResponse({ description: "Information successfully deleted " })
  deleteInformation(@Param("id") id: string) {
    return this.informationsService.delete(id);
  }
}
