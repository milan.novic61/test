import {
  Column,
  Default,
  Model,
  NotNull,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import { v4 as uuidv4 } from "uuid";

@Table({ modelName: "informations" })
export class Information extends Model {
  @Default(uuidv4)
  @PrimaryKey
  @Column
  id: string;

  @NotNull
  @Column({ allowNull: false })
  title: string;

  @NotNull
  @Column({ allowNull: false })
  description: string;
}
